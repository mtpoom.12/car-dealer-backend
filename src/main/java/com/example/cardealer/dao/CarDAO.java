package com.example.cardealer.dao;

import com.example.cardealer.model.Car;

import java.util.List;

public interface CarDAO {

    int save(Car car);

    int update(Car car,int id);

    int delete(int id);

    List<Car> getAll();

    Car getById(int id);

    List<Car> search(String str);
}

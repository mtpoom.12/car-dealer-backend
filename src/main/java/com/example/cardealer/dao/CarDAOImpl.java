package com.example.cardealer.dao;

import com.example.cardealer.model.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class CarDAOImpl implements CarDAO{

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String CAR_ID = "id";
    private final String CAR_NAME = "name";
    private final String CAR_MODEL = "model";
    private final String CAR_PRICE = "price";
    private final String CAR_COLOR = "color";
    private final String CAR_SEAT = "numberOfSeat";
    private final String CAR_TYPE = "type";
    private final String CAR_BRAND = "brand";
    private final String CAR_COUNTRY = "country";

    final RowMapper<Car> ROW_MAPPER = (ResultSet rs, int rowNum) -> {
        final Car carMapper = new Car();
        carMapper.setId(rs.getInt(CAR_ID));
        carMapper.setName(rs.getString(CAR_NAME));
        carMapper.setModel(rs.getString(CAR_MODEL));
        carMapper.setPrice(rs.getDouble(CAR_PRICE));
        carMapper.setColor(rs.getString(CAR_COLOR));
        carMapper.setNumberOfSeat(rs.getInt(CAR_SEAT));
        carMapper.setType(rs.getString(CAR_TYPE));
        carMapper.setBrand(rs.getString(CAR_BRAND));
        carMapper.setCountry(rs.getString(CAR_COUNTRY));
        return carMapper;
    };

    @Override
    public int save(Car car){
        int result = 0;
        try {
            result = jdbcTemplate.update("INSERT INTO car (name,model,price,color,numberOfSeat,type,brand,country)VALUES(?,?,?,?,?,?,?,?)",
                    new Object[]{car.getName(),car.getModel(),car.getPrice(),car.getColor(),car.getNumberOfSeat(),car.getType(),car.getBrand(),car.getCountry()});
        }catch (Exception e){
            System.out.println(e);
            throw e;
        }
        return result;
    }

    @Override
    public int update(Car car, int id) {
        int result = 0;
        try {
            result = jdbcTemplate.update("UPDATE car SET name=?,model=?,price=?,color=?,numberOfSeat=?,type=?,brand=?,country=? WHERE id=?",
                    new Object[]{car.getName(),car.getModel(),car.getPrice(),car.getColor(),car.getNumberOfSeat(),car.getType(),car.getBrand(),car.getCountry(),id});
        }catch (Exception e){
            System.out.println(e);
            throw e;
        }
        return result;
    }

    @Override
    public int delete(int id) {
        int result = 0;
        try {
            result = jdbcTemplate.update("DELETE FROM car WHERE id=?",id);
        }catch (Exception e){
            System.out.println(e);
            throw e;
        }
        return result;
    }

    @Override
    public List<Car> getAll() {
        List<Car> resultList;
        try {
            resultList = jdbcTemplate.query("SELECT * FROM car", ROW_MAPPER);
        }catch (Exception e){
            System.out.println(e);
            throw e;
        }
        return resultList;
    }

    @Override
    public Car getById(int id) {
        Car result;
        try {
            result = jdbcTemplate.queryForObject("SELECT * FROM car WHERE id=?",ROW_MAPPER,id);
        }catch (Exception e){
            System.out.println(e);
            throw e;
        }
        return result;
    }

    @Override
    public List<Car> search(String str){
        List<Car> resultList;
        try{
            resultList = jdbcTemplate.query("SELECT * FROM car \n" +
                            "WHERE name LIKE ? OR model LIKE ? \n" +
                            "OR price LIKE ? OR color LIKE ? \n" +
                            "OR numberOfSeat LIKE ? OR type LIKE ? \n" +
                            "OR brand LIKE ? OR country LIKE ?",ROW_MAPPER,
                    new Object[]{"%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%","%"+str+"%"});
        }catch (Exception e){
            System.out.println(e);
            throw e;
        }
        return resultList;
    }




}

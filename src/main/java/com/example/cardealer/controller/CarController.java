package com.example.cardealer.controller;

import com.example.cardealer.dao.CarDAO;
import com.example.cardealer.model.Car;
import com.example.cardealer.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin( origins = {"*"})
public class CarController extends CarService {
    Car car = new Car();

    @Autowired
    private CarDAO eDAO;


    @PostMapping("/cars")
    public List<Car> getCar(@RequestParam(value = "search", defaultValue = "")String str){
        if(!str.isEmpty()){
            return eDAO.search(str);
        }
        return eDAO.getAll();
    }

    @PostMapping("/cars/{id}")
    public Car getCarById(@PathVariable int id){return eDAO.getById(id);}

    @PostMapping("/addCars")
    public Boolean saveCar(@RequestBody Car car){
        if(validateTypeString(car.getName()) && validateTypeString(car.getModel()) && validateTypeString(car.getColor())
                && car.getNumberOfSeat() >0 && validateTypeString(car.getType()) && validateTypeString(car.getBrand())
                && validateTypeString(car.getCountry())){
            eDAO.save(car);
            return true;
        }
        return false;

    }

    @PostMapping("/updateCars/{id}")
    public boolean updateCar(@RequestBody Car car, @PathVariable int id){
        if(validateTypeString(car.getName()) && validateTypeString(car.getModel()) && validateTypeString(car.getColor())
                && car.getNumberOfSeat() >0 && validateTypeString(car.getType()) && validateTypeString(car.getBrand())
                && validateTypeString(car.getCountry()) && car.getId() > 0){
            eDAO.update(car,id);
            return true;
        }

        return false;

    }

    @PostMapping("/deleteCar/{id}")
    public String deleteCarById(@PathVariable int id){
        return eDAO.delete(id)+" Deleted";
    }
}

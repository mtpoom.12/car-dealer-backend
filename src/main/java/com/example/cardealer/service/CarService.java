package com.example.cardealer.service;

import com.example.cardealer.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CarService {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public boolean validateTypeString(String str){
        return !(Util.checkNull(str) || Util.trimLengthIsZero(str) || Util.lengthMoreThan(str,255) || checkSubstring(str));
    }

    public boolean checkSubstring(String str){
        Pattern p = Pattern.compile("[^A-Za-z0-9 ]");
        Matcher m = p.matcher(str);
        if(m.find()){
            return true; //Found Spacial Char
        }else{
            return false; // Not Found
        }
    }




}

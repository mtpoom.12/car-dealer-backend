package com.example.cardealer.util;



public class Util {
    public static boolean checkNull(String str) {
        return str == null;
    }
    public static boolean trimLengthIsZero(String str) {
        return  str.trim().length() == 0;
    }
    public static boolean lengthMoreThan(String str, int length){
        return str.length() > length;
    }
}